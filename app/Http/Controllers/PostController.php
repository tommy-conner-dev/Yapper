<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function deletePost(Post $post) {
        if (auth()->user()->id === $post['user_id']) {
            $post->delete();
        }
        // return redirect('/');
        return redirect()->back();
    }

    public function updatePost(Post $post, Request $request) {
        if(auth()->user()->id !== $post['user_id']) {
            // return redirect('/');
            return redirect()->back();
        }

        $incomingFields = $request->validate([
            
            'body' => 'required'
        ]);

        $incomingFields['body'] = strip_tags($incomingFields['body']);
        
        $post->update($incomingFields);
        // return redirect('/');
        return redirect('/feed');
    }
    public function showEditScreen(Post $post) {
        if(auth()->user()->id !== $post['user_id']) {
            // return redirect('/');
            return redirect()->back();
        }

        return view('edit-post', ['post' => $post]);
    }


    public function createPost(Request $request) {
        // $posts = Post::with('user')->get();
        $incomingFields = $request->validate([
            // 'title' => 'required',
            'body' => 'required'
        ]);

        // $incomingFields['title'] = strip_tags($incomingFields['title']);
        $incomingFields['body'] = strip_tags($incomingFields['body']);
        $incomingFields['user_id'] = auth()->id();
        Post::create($incomingFields);
        // return redirect('/');
        return redirect()->back();

    }

    public function editPost(Request $request) {
        // $posts = Post::with('user')->get();
        $incomingFields = $request->validate([
            // 'title' => 'required',
            'body' => 'required'
        ]);

        // $incomingFields['title'] = strip_tags($incomingFields['title']);
        $incomingFields['body'] = strip_tags($incomingFields['body']);
        $incomingFields['user_id'] = auth()->id();
        Post::create($incomingFields);
        // return redirect('/');
        return redirect()->back();

    }

    public static function getUsername($userId) {
        return \DB::table('users')->where('id', $userId)->first()->name;
    }
}
