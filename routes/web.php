<?php

use App\Models\Post;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    $allposts = Post::all()->sortDesc();
    return redirect('/feed');
});

Route::get('/my-yaps', function () {
    $posts = [];
    if (auth()->check()) {
        $posts = auth()->user()->usersPosts()->latest()->get();
    }
    return view('my-yaps',['posts' => $posts]);
})->middleware(['auth', 'verified'])->name('my-yaps');

// Route::post('/register', [UserController::class, 'register']);
// Route::post('/logout', [UserController::class, 'logout']);
// Route::post('/login', [UserController::class, 'login']);

//Blog Post Routes
// Route::post('/create-post', [PostController::Class, 'createPost']);
// Route::get('/edit-post/{post}', [PostController::Class, 'showEditScreen']);
// Route::put('/edit-post/{post}', [PostController::Class, 'updatePost']);
// Route::delete('/delete-post/{post}', [PostController::Class, 'deletePost']);
Route::get('/feed', function () {
    $allposts = Post::all()->sortDesc();
    return view('feed', ['allposts' => $allposts]);
})->middleware(['auth', 'verified'])->name('feed');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::post('/create-post', [PostController::Class, 'createPost']);
Route::get('/edit-post/{post}', [PostController::Class, 'showEditScreen']);
Route::put('/edit-post/{post}', [PostController::Class, 'updatePost']);
Route::delete('/delete-post/{post}', [PostController::Class, 'deletePost']);
});

require __DIR__.'/auth.php';