@php
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
@endphp
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>yapper!</title>
        @vite('resources/css/app.css')
    </head>
    <body>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('my feed') }}
        </h2>
        
    </x-slot>

    @auth
        <x-yap-box/>
        <div class="flex flex-col mx-5">
            <h2 class="pl-5 pb-1 text-gray-800 dark:text-gray-200 text-3xl">
                what's yappin
            </h2><hr class="h-px my-8 bg-gray-200 border-0 dark:bg-gray-700">
            @foreach($allposts as $post)
            <div style="background-color:aquamarine; padding:1em; margin: 1em; border-radius:0.5em;">
            <p>{{$post['body']}}</p>
            @php
            $user = Auth::user();
            $userid = Auth::id();
            @endphp
            <i>Author:{{PostController::getUsername($post['user_id'])}}</i>
            @if ($post['user_id'] == $userid)
            <p><a href="/edit-post/{{$post->id}}">edit yap</a></p>
            <form action="/delete-post/{{$post->id}}" method="POST">
                @csrf
                @method('DELETE')
                <button>Delete</button>
            </form>
            @endif
            
            <p>Date: {{Carbon::parse($post['created_at'])->format('m-d-y');}} </p>
            <p>Time: {{Carbon::parse($post['created_at'])->format('g:i A');}} </p>
            <svg class="h-8 w-8 text-violet-500"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round">  <path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3" /></svg>
            </div>
        
            @endforeach
        </div>
    @else
    
    @endauth


</body>
</html>
</x-app-layout>
