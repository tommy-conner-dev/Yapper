@php
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Auth;
@endphp
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{Auth::user()->name}}'s yaps | yapper</title>
        @vite('resources/css/app.css')
    </head>
    <body>
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{Auth::user()->name}}'s yaps
        </h2>
        
    </x-slot>

    @auth
        <x-yap-box/>

        <div class="flex flex-col mx-5">
            <h2 class="pl-5 pb-1 text-gray-800 dark:text-gray-200 text-3xl">
                my yaps
            </h2><hr class="h-px my-8 bg-gray-200 border-0 dark:bg-gray-700">
            @foreach($posts as $post)
            <div style="background-color:aquamarine; padding:1em; margin: 1em; border-radius:0.5em;">
            <p>{{$post['body']}}</p>
            @php
            $user = Auth::user();
            $userid = Auth::id();
            @endphp
            <i>Author:{{PostController::getUsername($post['user_id'])}}</i>
            @if ($post['user_id'] == $userid)
            <p><a href="/edit-post/{{$post->id}}">edit yap</a></p>
            <form action="/delete-post/{{$post->id}}" method="POST">
                @csrf
                @method('DELETE')
                <button>Delete</button>
            </form>
            @endif
            </div>

            @endforeach
        </div>
    @else
    
    @endauth


</body>
</html>
</x-app-layout>
