<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>edit yap</title>
    @vite('resources/css/app.css')
</head>
<body>
    <x-app-layout>
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
                {{ __('edit yap') }}
            </h2>

        </x-slot>
    @auth
    <div class="flex flex-col items-center">
    <h2 class="text-gray-800 dark:text-gray-200 text-3xl">uhh... what i meant was</h2>
        <form class="min-w-80 flex flex-col" action="/edit-post/{{$post->id}}" method="POST">
        @csrf
        @method('PUT')
        <textarea name="body" >{{$post->body}}</textarea>
        <button class="my-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">save yap</button>
        </form>
    </div>
    @else
    <p>How did you get here? That's not allowed!</p>
    @endauth
</body>
</html>
</x-app-layout>