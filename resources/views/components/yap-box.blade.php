<div class="flex flex-col items-center">
    <h2 class="py-2 text-gray-800 dark:text-gray-200 text-3xl">got somethin' to say?</h2>
    <form class="min-w-80 flex flex-col" action="/create-post" method="POST">
        @csrf
        <textarea name="body" placeholder="what's on your mind?"></textarea>
        <button class="my-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">yap it</button>
    </form>
</div>